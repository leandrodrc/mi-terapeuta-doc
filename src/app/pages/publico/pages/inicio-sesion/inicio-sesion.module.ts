import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

import {DialogModule} from "primeng/dialog";
import {ButtonModule} from "primeng/button";

import {InicioSesionComponent} from "./inicio-sesion.component";
import {InicioSesionRouting} from "./inicio-sesion.routing";

@NgModule({
  declarations:[InicioSesionComponent],
  imports:[
    CommonModule,
    InicioSesionRouting,
    ReactiveFormsModule,
    DialogModule,
    ButtonModule
  ]
})
export class InicioSesionModule{}

import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {PublicoComponent} from "./publico.component";
import {PublicoRoutingModule} from "./publico-routing.module";

@NgModule({
  declarations:[PublicoComponent],
  imports:[CommonModule,PublicoRoutingModule]
})

export class PublicoModule{}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'publico',
    pathMatch: 'full'
  },
  {
    path: 'publico',
    loadChildren: () =>
      import('./pages/publico/publico.module').then((m) => m.PublicoModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
